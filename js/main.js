// Exercise 1

var ex1HanderlerBtn = document.getElementById("ex1-handler-btn");

function ex1Handler(){
    var resultHtml = "";
    var numCount = 1;
    for(i = 1 ; i <= 10 ; i++){
        resultHtml = resultHtml +"<tr>"
        for(j = 1 ; j <= 10; j++){
            resultHtml += `<td>${numCount}</td>`
            numCount++;
        }
        resultHtml += "</tr>"
    }
    document.querySelector('#ex1-result #ex-result-text').innerHTML = resultHtml;
}

// Exercise 2
var resultEx2 =""
var ex2Arr = [];
var ex2ShowArr = document.getElementById('ex2-array-display');
function ex2addItem(){
    var ex2InputItem = document.getElementById('ex2IputItem').value;
    if(ex2InputItem != "" && Number.isInteger(ex2InputItem*1)){
        ex2Arr.push(ex2InputItem*1);
        ex2ShowArr.innerHTML = "[" + ex2Arr + "]";
    }
}

function isPrimeNum(num){
    if(num>1){
        for(var i = 2; i < num; i++){
            if(num%i==0){
                return false;
            }
        }
         return true;
    }
}

function Ex2deleteArr(){
    ex2Arr =[];
    ex2ShowArr.innerHTML = ex2Arr;
}


function ex2Handler(){
    var resultArr = ex2Arr.filter((item)=>{
        return isPrimeNum(item);
    })
    if(resultArr.length !=0){
        resultEx2 = resultArr;
    } else {
        resultEx2 = "Chuỗi không chứ số nguyên tố!"
    }

    document.querySelector('#ex2-result #ex-result-text').innerHTML = resultEx2;
}
// Exercise 3

function ex3Handler(){
    var ex3Input1 = document.querySelector("#ex3-input #ex-input-1").value*1;
    var n = ex3Input1;
    var sum = 0;
    if(n>=2){
        for ( var i = 2 ; i <= n ; i++){
            sum += i ;
        }
        sum += 2*n;
    }
    var resultEx3 = sum;
    
    document.querySelector('#ex3-result #ex-result-text').innerHTML = resultEx3;
}

// Exercise 4

var ex4HanderlerBtn = document.getElementById("ex4-handler-btn");

function ex4Handler(){

    var ex4Input1 = document.querySelector("#ex4-input #ex-input-1").value;

    var n = ex4Input1;
    var resultArr = [];
    for(i = 1; i <= n ; i++){
        if(n%i==0){
            resultArr.push(i);
        }
    }


    var resultEx4 = resultArr;
    document.querySelector('#ex4-result #ex-result-text').innerHTML = resultEx4;
}


// Exercise 5

function ex5Handler(){
    var ex5Input1 = document.querySelector("#ex5-input #ex-input-1").value;
    var n = ex5Input1;
    if(n!=""){
        var choppedStringArr = n.split("");
        var invertStringArr = [];
        for(i = 0; i < choppedStringArr.length; i++){
            invertStringArr[i] = choppedStringArr[choppedStringArr.length -i-1];
        }
    var resultString = invertStringArr.join('');
    document.querySelector('#ex5-result #ex-result-text').innerHTML = resultString;
    }   
}

// Exercise 6

function ex6Handler(){
    var sum = 0;
    for(i = 0 ; sum <= 100; i++){
        sum +=i;
    }
    var maxI = i -1;
    document.querySelector('#ex6-result #ex-result-text').innerHTML = maxI;
}

// Exercise 7

function ex7Handler(){
    var ex7Input1 = document.querySelector("#ex7-input #ex-input-1").value*1;
    var n = ex7Input1;
    var resultHTML ="";
    for (i = 0 ; i <= 10; i++){
        resultHTML = resultHTML + `
        <p>${n} x ${i} = ${n*i}</p>
        `;
    }
    document.querySelector('#ex7-result #ex-result-text').innerHTML = resultHTML;
}

// Exercise 8

function ex8Handler(){
    var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];
    var players = [[],[],[],[]];
    var index = 0;
    for ( i = 0 ; i < 3 ; i++){
        for( j = 0 ; j < players.length ; j++ ){
            players[j].push(cards[index]);
            index++;
        }
    }
    var resultHTML ="";
    for(k = 0; k < players.length; k++){
        resultHTML += `
        <p>player${k+1} = [${players[k]}]</p>
        `;
    }
    document.querySelector('#ex8-result #ex-result-text').innerHTML = resultHTML;
}

// Exercise 9

function ex9Handler(){
    var ex9Input1 = document.querySelector("#ex9-input #ex-input-1").value*1;
    var ex9Input2 = document.querySelector("#ex9-input #ex-input-2").value*1;
    var tongGaVaCho = ex9Input1;
    var tongChan = ex9Input2;
    var x = 0,y=0;
    var resultArr = [[]];
    var k = 0;
    for ( x = 0 ; x < tongGaVaCho;x++){
        y = tongGaVaCho - x;
        if(2*x + 4*y == tongChan){
            resultArr[k].push(x,y);
            k++;
        }
    }
    var resultHTML = "";
    
    console.log(resultArr[0]);
    if(resultArr[0].length !=0){
        resultHTML = " [Số gà, số chó] = ";
        for (j = 0; j<resultArr.length;j++){
            resultHTML = resultHTML + "[" + resultArr[j] +"] ";
        }
    } else {
        resultHTML = "Không tìm thấy số lượng gà, chó phù hợp."
    }
    document.querySelector('#ex9-result #ex-result-text').innerHTML = resultHTML;
}

// Exercise 10

function ex10Handler(){
    var ex10Input1 = document.querySelector("#ex10-input #ex-input-1").value;
    var ex10Input2 = document.querySelector("#ex10-input #ex-input-2").value;
    var hour = ex10Input1*1;
    var minute = ex10Input2*1;
    var hourHandPosition = 0;
    var minuteHandPosition = 0;
    var angle = 0;
    var resultStr = "";


    if(hour > 23 || hour <0 || minute < 0 || minute > 59){
        resultStr = "Hãy nhập giờ : phút hợp lệ!"
    } else {
        if(hour > 12) {
            hour = hour - 12;
        }
        if ( hour == 12){
            hourHandPosition = minute*0.5;
        } else {
            hourHandPosition = hour*30 + minute*0.5;
        }
        minuteHandPosition = minute*6;
    
        angle = Math.abs(hourHandPosition - minuteHandPosition);
        drawFace(ctx, radius);
        drawNumbers(ctx, radius);
        drawTime(ctx, radius,hour,minute);
        resultStr = "Góc giữa kim giờ và kim phút : " + angle +"&deg;";
    }
    
    document.querySelector('#ex10-result #ex-result-text').innerHTML = resultStr;
}